import java.util.Scanner;

/**
 * Write a description of class ConverterApp here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */




public class ConverterApp
{
    // instance variables - replace the example below with your own
    Scanner userInput;
    Float userCurrentCurrency;
    Float userMoney;
    Float userNewCurrency;
    String userNewCurrencyString;
    
    NZD userNZD;
    AUD userAUD;
    USD userUSD;
    GBP userGBP;
    JPY userJPY;
    IRR userIRR;
    
    /**
     * Constructor for objects of class ConverterApp
     */
    public ConverterApp()
    {
        userInput = new Scanner(System.in);
        userNZD = new NZD();
        userAUD = new AUD();
        userUSD = new USD();
        userGBP = new GBP();
        userJPY = new JPY();
        userIRR = new IRR();
    }

    public void start(){
        
        System.out.println("\nWhat currency do you currently have?\n");
        System.out.println("1. NZD");
        System.out.println("2. AUD");
        System.out.println("3. USD");
        System.out.println("4. GBP");
        System.out.println("5. JPY");
        System.out.println("6. IRR");
        
        try{
            // set userCurrentCurrency
            getCurrentCurrency();
        }
        catch(Exception e){
            System.out.println("That's not a currency bro");
            System.exit(0);
        }
        
        System.out.println("\nHow much of that currency do you want to convert?\n");
        
        try{
            // set userMoney
            getUserMoney();
        }
        catch(Exception e){
            System.out.println("Doesn't look like a proper number to me");
            System.exit(0);
        }
        
        System.out.println("\nWhat currency do you want to convert to?\n");
        System.out.println("1. NZD");
        System.out.println("2. AUD");
        System.out.println("3. USD");
        System.out.println("4. GBP");
        System.out.println("5. JPY");
        System.out.println("6. IRR");
        
        try{
            // set userNewCurrency
            getNewCurrency();
        }
        catch(Exception e){
            System.out.println("That's not a currency you can convert to");
            System.exit(0);
        }
        
        //setString for map key
        if(userNewCurrency == 1){
            userNewCurrencyString = "nzdRate";
        }
        else if(userNewCurrency == 2){
            userNewCurrencyString = "audRate";
        }
        else if(userNewCurrency == 3){
            userNewCurrencyString = "usdRate";
        }
        else if(userNewCurrency == 4){
            userNewCurrencyString = "gbpRate";
        }
        else if(userNewCurrency == 5){
            userNewCurrencyString = "jpyRate";
        }
        else if(userNewCurrency == 6){
            userNewCurrencyString = "irrRate";
        }
        else{
            System.out.print("Not one of the options (in if statement)");
            System.exit(0);
        }
        
        //calculate current currency * conversion rate * current currency value
        if(userCurrentCurrency == 1){
            float result = userNZD.getRate(userNewCurrencyString);
            result = result * userMoney;
            System.out.println("\nConverting your NZD at the " + userNewCurrencyString + " gives you " + result);
        }
        else if(userCurrentCurrency == 2){
            float result = userAUD.getRate(userNewCurrencyString);
            result = result * userMoney;
            System.out.println("\nConverting your AUD at the " + userNewCurrencyString + " gives you " + result);
        }
        else if(userCurrentCurrency == 3){
            float result = userUSD.getRate(userNewCurrencyString);
            result = result * userMoney;
            System.out.println("\nConverting your USD at the " + userNewCurrencyString + " gives you " + result);
        }
        else if(userCurrentCurrency == 4){
            float result = userGBP.getRate(userNewCurrencyString);
            result = result * userMoney;
            System.out.println("\nConverting your GBP at the " + userNewCurrencyString + " gives you " + result);
        }
        else if(userCurrentCurrency == 5){
            float result = userJPY.getRate(userNewCurrencyString);
            result = result * userMoney;
            System.out.println("\nConverting your JPY at the " + userNewCurrencyString + " gives you " + result);
        }
        else if(userCurrentCurrency == 6){
            float result = userIRR.getRate(userNewCurrencyString);
            result = result * userMoney;
            System.out.println("\nConverting your IRR at the " + userNewCurrencyString + " gives you " + result);
        }
        else{
            System.out.print("That's not an option my guy");
            System.exit(0);
        }
        
        start();
    }

    void getCurrentCurrency(){
        userCurrentCurrency = userInput.nextFloat();
    }
    
    void getUserMoney(){
        userMoney = userInput.nextFloat();
    }
    
    void getNewCurrency(){
        userNewCurrency = userInput.nextFloat();
    }
}
