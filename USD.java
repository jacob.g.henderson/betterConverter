import java.util.HashMap;

/**
 * Write a description of class USD here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class USD
{
    // instance variables - replace the example below with your own
    public String currencyName = "USD";
    public HashMap<String, Float> conversionRate = new HashMap<>();
    
    
        public USD(){
        conversionRate.put("usdRate", 1.00f);    
        conversionRate.put("audRate", 1.39f);
        conversionRate.put("nzdRate", 1.49f);
        conversionRate.put("gbpRate", 0.74f);
        conversionRate.put("jpyRate", 115.82f);
        conversionRate.put("irrRate", 42250.00f);
    }
    
    Float getRate(String currency){
        Float testConversion = 0.00f;
        testConversion = conversionRate.get(currency);
        return testConversion;
    }
}
