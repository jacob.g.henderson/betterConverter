import java.util.HashMap;

/**
 * Write a description of class AUD here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class AUD
{
    // instance variables - replace the example below with your own
    public String currencyName = "AUD";
    public HashMap<String, Float> conversionRate = new HashMap<>();
    
    
        public AUD(){
        conversionRate.put("audRate", 1.00f);
        conversionRate.put("nzdRate", 1.07f);
        conversionRate.put("usdRate", 0.72f);
        conversionRate.put("gbpRate", 0.53f);
        conversionRate.put("jpyRate", 83.35f);
        conversionRate.put("irrRate", 30407.32f);
    }
    
    Float getRate(String currency){
        Float testConversion = 0.00f;
        testConversion = conversionRate.get(currency);
        return testConversion;
    }
}
