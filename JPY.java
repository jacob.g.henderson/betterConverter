import java.util.HashMap;

/**
 * Write a description of class JPY here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class JPY
{
    public String currencyName = "NZD";
    public HashMap<String, Float> conversionRate = new HashMap<>();
    
    
        public JPY(){
        conversionRate.put("jpyRate", 1.00f);    
        conversionRate.put("audRate", 0.012f);
        conversionRate.put("usdRate", 0.0086f);
        conversionRate.put("gbpRate", 0.0064f);
        conversionRate.put("nzdRate", 0.013f);
        conversionRate.put("irrRate", 364.78f);
    }
    
    Float getRate(String currency){
        Float testConversion = 0.00f;
        testConversion = conversionRate.get(currency);
        return testConversion;
    }
}
