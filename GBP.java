import java.util.HashMap;

/**
 * Write a description of class GBP here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class GBP
{
    public String currencyName = "GBP";
    public HashMap<String, Float> conversionRate = new HashMap<>();
    
    
        public GBP(){   
        conversionRate.put("gbpRate", 1.00f);
        conversionRate.put("audRate", 1.88f);
        conversionRate.put("usdRate", 1.36f);
        conversionRate.put("nzdRate", 2.02f);
        conversionRate.put("jpyRate", 157.09f);
        conversionRate.put("irrRate", 57308.96f);
    }
    
    Float getRate(String currency){
        Float testConversion = 0.00f;
        testConversion = conversionRate.get(currency);
        return testConversion;
    }
}
