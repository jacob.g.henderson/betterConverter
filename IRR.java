import java.util.HashMap;

/**
 * Write a description of class IRR here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class IRR
{
    public String currencyName = "NZD";
    public HashMap<String, Float> conversionRate = new HashMap<>();
    
    
        public IRR(){
        conversionRate.put("irrRate", 1.00f);    
        conversionRate.put("audRate", 0.000033f);
        conversionRate.put("usdRate", 0.000024f);
        conversionRate.put("gbpRate", 0.000017f);
        conversionRate.put("jpyRate", 0.0027f);
        conversionRate.put("nzdRate", 0.000035f);
    }
    
    Float getRate(String currency){
        Float testConversion = 0.00f;
        testConversion = conversionRate.get(currency);
        return testConversion;
    }
}
