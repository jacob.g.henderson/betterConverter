import java.util.HashMap;

/**
 * Write a description of class NZD here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class NZD
{
    // instance variables - replace the example below with your own
    public String currencyName = "NZD";
    public HashMap<String, Float> conversionRate = new HashMap<>();
    
    
    public NZD(){  
        conversionRate.put("nzdRate", 1.00f);
        conversionRate.put("audRate", 0.93f);
        conversionRate.put("usdRate", 0.67f);
        conversionRate.put("gbpRate", 0.49f);
        conversionRate.put("jpyRate", 77.36f);
        conversionRate.put("irrRate", 28265.88f);
    }
    
    public Float getRate(String currency){
        Float testConversion = 0.00f;
        testConversion = conversionRate.get(currency);
        return testConversion;
    }

}
